//
//  LoginViewModel.swift
//  WeatherApp
//
//  Created by mac on 27/05/2023.
//

import Foundation

enum LoginStatus {
    case idle
    case loading
    case success
    case failure(String) // Updated to include the error message
}


class LoginViewModel: ObservableObject{
        @Published var username = ""
        @Published var password = ""
        @Published var loginStatus: LoginStatus = .idle
        private let loginUseCase: LoginUseCase
        init(loginUseCase: LoginUseCase) {
            self.loginUseCase = loginUseCase
        }
    
    func login() {
           loginStatus = .loading
           
           loginUseCase.login(username: username, password: password) { [weak self] result in
               switch result {
               case .success:
                   self?.loginStatus = .success
                   
               case .failure(let error):
                   self?.loginStatus = .failure(error.localizedDescription)
               }
           }
       }
}
