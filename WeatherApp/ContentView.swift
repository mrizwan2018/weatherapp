//
//  ContentView.swift
//  WeatherApp
//
//  Created by mac on 27/05/2023.
//

import SwiftUI

struct ContentView: View {
    
    @EnvironmentObject var loginViewModel: LoginViewModel
    
    var body: some View {
        LoginView(viewModel: loginViewModel)
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
