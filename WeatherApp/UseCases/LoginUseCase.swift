//
//  LoginUseCase.swift
//  WeatherApp
//
//  Created by mac on 27/05/2023.
//

import Foundation
class LoginUseCase {
    private let loginApi: LoginAPI
    
    init(loginApi: LoginAPI) {
        self.loginApi = loginApi
    }
    
    func login(username: String, password: String, completion: @escaping (Result<Void, Error>) -> Void) {
        loginApi.authenticate(username: username, password: password) { result in
            switch result {
            case .success:
                completion(.success(()))
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}

