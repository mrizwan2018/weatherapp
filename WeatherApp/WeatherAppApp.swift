//
//  WeatherAppApp.swift
//  WeatherApp
//
//  Created by mac on 27/05/2023.

import SwiftUI
@main

struct WeatherAppApp: App {
    let baseURL = URL(string: "https://run.mocky.io/v3")!
       @StateObject var loginViewModel: LoginViewModel

       init() {
           let apiService = APIService(baseURL: baseURL)
           let loginApi = LoginAPI(apiService: apiService)
           let loginUseCase = LoginUseCase(loginApi: loginApi)
           _loginViewModel = StateObject(wrappedValue: LoginViewModel(loginUseCase: loginUseCase))
    }

    var body: some Scene {
        WindowGroup {
            NavigationView {
                LoginView(viewModel: loginViewModel)
            }
        }
    }

}

