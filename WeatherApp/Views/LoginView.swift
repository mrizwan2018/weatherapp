//
//  LoginView.swift
//  WeatherApp
//
//  Created by mac on 27/05/2023.
//

import SwiftUI

struct LoginView: View {
    @ObservedObject var viewModel: LoginViewModel
    
    var body: some View {
        VStack {
            TextField("Username", text: $viewModel.username)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()
            
            SecureField("Password", text: $viewModel.password)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()
            
            Button("Login") {
                viewModel.login()
            }
            .padding()
            
            loginStatusText
        }
        .padding()
    }
    
    var loginStatusText: some View {
        switch viewModel.loginStatus {
        case .idle:
            return Text("")
        case .loading:
            return Text("Logging in...")
        case .success:
            return Text("Login successful")
        case .failure(let errorMessage):
            return Text("Login failed: \(errorMessage)")
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        let apiService = APIService(baseURL: URL(string: "https://run.mocky.io/v3")!)
        let loginApi = LoginAPI(apiService: apiService)
        let loginUseCase = LoginUseCase(loginApi: loginApi)
        let loginViewModel = LoginViewModel(loginUseCase: loginUseCase)
        return LoginView(viewModel: loginViewModel)
            .environmentObject(apiService) // Pass the apiService to the view
    }
}

