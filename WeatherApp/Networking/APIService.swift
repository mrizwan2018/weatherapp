//
//  NetworkingClient.swift
//  WeatherApp
//
//  Created by mac on 27/05/2023.
//

import Foundation
class APIService : ObservableObject {
    private let baseURL: URL
    
    init(baseURL: URL) {
        self.baseURL = baseURL
    }
    
    func request(endpoint: String, method: String, parameters: [String: Any]?, completion: @escaping (Result<Data?, Error>) -> Void) {
        guard let url = URL(string: endpoint, relativeTo: baseURL) else {
            let error = NSError(domain: "YourApp.APIService", code: 0, userInfo: [NSLocalizedDescriptionKey: "Invalid URL"])
            completion(.failure(error))
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = method
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        if let parameters = parameters {
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters)
            } catch {
                completion(.failure(error))
                return
            }
        }
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            completion(.success(data))
        }
        task.resume()
    }
}

