//
//  WeatherEntity.swift
//  WeatherApp
//
//  Created by mac on 27/05/2023.
//

import Foundation

struct WeatherEntity {
    let temperature: Double
    let humidity: Double
    let windSpeed: Double
    let description: String
}
