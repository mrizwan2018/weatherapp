//
//  LoginEntity.swift
//  WeatherApp
//
//  Created by mac on 27/05/2023.
//

import Foundation

struct LoginEntity{
    let userName: String
    let password: String 
}
