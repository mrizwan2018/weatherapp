//
//  LoginAPI.swift
//  WeatherApp
//
//  Created by mac on 27/05/2023.
//

import Foundation
class LoginAPI {
    private let apiService: APIService
    
    init(apiService: APIService) {
        self.apiService = apiService
    }
    
    func authenticate(username: String, password: String, completion: @escaping (Result<Void, Error>) -> Void) {
        let endpoint = "/edb4e63a-b40d-40cf-b9a1-f4687a7e460f" // Update with the actual authentication endpoint
        
        let parameters: [String: Any] = [
            "username": username,
            "password": password
        ]
        
        apiService.request(endpoint: endpoint, method: "POST", parameters: parameters) { result in
            switch result {
            case .success:
                completion(.success(()))
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}



